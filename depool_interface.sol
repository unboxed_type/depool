pragma solidity >=0.6.0;

enum RoundStep {
    // Receiving half of vesting/lock stake from participants
    PrePooling, // 0

    // Receiving stakes from participants
    Pooling, // 1

    // Waiting for election request from validator
    WaitingValidatorRequest, // 2
    // Stake has been sent to elector. Waiting for answer from elector.
    WaitingIfStakeAccepted, // 3

    // Elector has accepted round stake. Validator is candidate. Waiting validation period to know if we win elections
    WaitingValidationStart, // 4
    // DePool has tried to recover stake in validation period to know if we won elections. Waiting for elector answer
    WaitingIfValidatorWinElections, // 5
    // If CompletionReason!=Undefined, then round is completed and waiting to return/reinvest funds after the next round.
    // Else validator won elections. Waiting for the end of unfreeze period
    WaitingUnfreeze, // 6
    // Unfreeze period has been ended. Request to recover stake has been sent to elector. Waiting for answer from elector.
    WaitingReward, // 7

    // Returning or reinvesting participant stakes because round is completed
    Completing, // 8
    // All round's states are returned or reinvested
    Completed // 9
}

// Round completion statuses. Filled when round is switched to 'WaitingUnfreeze' or 'Completing' step.
enum CompletionReason {
    // Round is not completed yet
    Undefined, // 0
    // DePool is closed
    PoolClosed, // 1
    // Fake round. Used in constructor to create round1 and round2
    FakeRound, // 2
    // Validator stake is less than m_validatorAssurance
    ValidatorStakeIsTooSmall, // 3
    // Stake is rejected by elector for some reason
    StakeIsRejectedByElector, // 4
    // Reward is received from elector. Round is completed successfully
    RewardIsReceived, // 5
    // DePool has participated in elections but lost the elections
    ElectionsAreLost, // 6
    // Validator is blamed during investigation phase
    ValidatorIsPunished, // 7
    // Validator sent no request during election phase
    NoValidatorRequest // 8
}

  // Describes vesting or lock stake
struct InvestParams {
    // Size of vesting stake
    uint64 amount;
    // Unix time in seconds of last payment
    uint64 lastWithdrawalTime;
    // Period in seconds after which `withdrawalValue` nanotons are unlocked
    uint32 withdrawalPeriod;
    // Amount of nanotons which are unlocked after `interval` second
    uint64 withdrawalValue;
    // Address of creator of vesting/lock
    address owner;
}

struct TruncatedRound {
  uint64 id;
  uint32 supposedElectedAt;
  uint32 unfreeze;
  uint32 stakeHeldFor;
  uint256 vsetHashInElectionPhase;
  RoundStep step;
  CompletionReason completionReason;

  uint64 stake;
  uint64 recoveredStake;
  uint64 unused;
  bool isValidatorStakeCompleted;
  uint64 rewards;
  uint32 participantQty;
  uint64 validatorStake;
  uint64 validatorRemainingStake;
  uint64 handledStakesAndRewards;
}

interface DePool {
  /*  
  constructor(
	      uint64 minStake,
	      uint64 validatorAssurance,
	      TvmCell proxyCode,
	      address validatorWallet,
	      uint8 participantRewardFraction,
	      uint64 balaceThreshold
	      ) external; */
  function addOrdinaryStake(uint64 stake) external;
  function addVestingStake(uint64 stake,
			   address beneficiary,
			   uint32 withdrawalPeriod,
			   uint32 totalPeriod) external;
  function addLockStake(uint64 stake,
			address beneficiary,
			uint32 withdrawalPeriod,
			uint32 totalPeriod) external;
  function withdrawPart(uint64 withdrawValue) external;
  function withdrawAll() external;
  function withdrawFromPoolingRound(uint64 withdrawValue) external;
  function cancelWithdrawal() external;
  function transferStake(address dest,
			 uint64 amount) external;
  function participateInElections(uint64 queryId,
				  uint256 validatorKey,
				  uint32 stakeAt,
				  uint32 maxFactor,
				  uint256 adnlAddr,
				  bytes signature) external;
  function ticktock() external;
  function completeRoundWithChunk(uint64 roundId,
				  uint8 chunkSize) external;
  function completeRound(uint64 roundId,
			 uint32 participantQty) external;
  function onStakeAccept(uint64 queryId,
			 uint32 comment,
			 address elector) external;
  function onStakeReject(uint64 queryId,
			 uint32 comment,
			 address elector) external;
  function onSuccessToRecoverStake(uint64 queryId,
				   address elector) external;
  function onFailToRecoverStake(uint64 queryId,
				address elector) external;
  function terminator() external;
  function receiveFunds() external;
  function getLastRoundInfo() external view;
  function getParticipantInfo(address addr) external view
    returns (uint64 total,
	     uint64 withdrawValue,
	     bool reinvest,
	     uint64 reward,
	     mapping (uint64 => uint64) stakes,
	     mapping (uint64 => InvestParams) vesting,
	     mapping (uint64 => InvestParams) locks);
  function getDePoolInfo() external view
    returns (bool poolClosed,
	     uint64 minStake,
	     uint64 validatorAssurance,
	     uint8 participantRewardFraction,
	     uint8 validatorRewardFraction,
	     uint64 balaceThreshold,
	     address validatorWallet,
	     address[] proxies,
	     uint64 stakeFee,
	     uint64 retOrReinvFee,
	     uint64 proxyFee);
  function getParticipants() external view
    returns (address[] participants);
  function getRounds() external view
    returns (mapping (uint64 => TruncatedRound));

  event DePoolClosed();
  event RoundStakeIsAccepted(uint64 queryId, uint32 comment);
  event RoundStakeIsRejected(uint64 queryId, uint32 comment);
  event ProxyHasRejectedTheStake(uint64 queryId);
  event ProxyHasRejectedRecoverRequest(uint64 roundId);
  event RoundCompleted(TruncatedRound r);
  event StakeSigningRequested(uint32 electionId, address proxy);
  event TooLowDePoolBalance(uint256 replenishment);
}
